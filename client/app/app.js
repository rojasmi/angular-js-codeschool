(function(){
  var gems = [
    {
    name: 'Azurite',
    price: 2.95,
    description: 'Beautiful Gem',
    canPurchase: false,
    soldOut: false
    },
    {
    name: 'Bloodstone',
    price: 5.95,
    description: 'Beautiful Gem',
    canPurchase: false,
    soldOut: false
    },
    {
    name: 'Zircon',
    price: 3.95,
    description: 'Beautiful Gem',
    canPurchase: false,
    soldOut: false
    },
  ];
  var app = angular.module('gemStore', []);

  app.controller('StoreController',function(){
    this.products = gems;
  });
})();
